import FhirExtender from '../src/FhirExtender';

describe('tests the FhirExtender', () => {
    describe('tests adding defaults to FhirExtender', () => {
        test('tests addDefault() with valid values', () => {
            testAddingDefault(
                'Patient',
                { name: 'Max' },
                { Patient: { name: 'Max' } }
            );
        });

        test('tests addDefault() with null or undefined', () => {
            testAddingDefault(undefined, { id: '3045' }, {});
            testAddingDefault(null, { id: '3045' }, {});
            testAddingDefault('Patient', undefined, {});
            testAddingDefault('Patient', null, {});
        });

        test('tests addDefaults() with valid values', () => {
            const mapping = {
                Patient: { name: 'Max' },
                Observation: { id: '3045' },
            };
            testAddingDefaults(mapping, mapping);
        });

        test('tests addDefaults() with null or undefined', () => {
            testAddingDefaults(null, {});
            testAddingDefaults(undefined, {});
        });
    });

    describe('tests extending and minimizing resources', () => {
        let fhirExtender: FhirExtender;

        const patientDefaults = {
            name: {
                use: 'official',
                family: 'Mustermann',
                given: ['Max'],
            },
        };

        const observationDefaults = {
            valueQuantity: {
                value: 90,
                unit: 'bpm',
            },
        };

        beforeEach(() => {
            fhirExtender = createFhirExtenderWithDefaults({
                Patient: patientDefaults,
                Observation: observationDefaults,
            });
        });

        test('tests whether extending a resource that does not include the values set in defaults works', () => {
            const resource = { id: '3045' };
            testExtendingResource(fhirExtender, 'Patient', resource, {
                ...resource,
                ...patientDefaults,
            });
        });

        test('tests whether extending a resource that does include the value set in defaults does nothing', () => {
            const resource = { id: '3045', name: { family: 'Paul' } };
            testExtendingResource(fhirExtender, 'Patient', resource, resource);
        });

        test("test extending resource where there aren't any defaults saved for the resource type", () => {
            const resource = { id: '3045' };
            testExtendingResource(
                fhirExtender,
                'Practicioner',
                resource,
                resource
            );
        });

        test('tests that minimizing resources which has the same key and value as the defaults is minimized correctly', () => {
            const resource = { id: '3045' };
            testMinimizingResource(
                fhirExtender,
                'Patient',
                { ...resource, ...patientDefaults },
                resource
            );
        });

        test('tests that a resource which contains the same key but not the same value is not minimized', () => {
            const resource = { id: '3045', name: { family: 'Paul' } };
            testMinimizingResource(fhirExtender, 'Patient', resource, resource);
        });

        test("test minimizing resource where there aren't any defaults saved for the resource type", () => {
            const resource = { id: '3045' };
            testMinimizingResource(
                fhirExtender,
                'Practicioner',
                resource,
                resource
            );
        });
    });
});

/** Helper method for testing extending a resource using {@link FhirExtender}. */
function testExtendingResource(
    fhirExtender: FhirExtender,
    resourceType: string,
    resource: any,
    expectedObject: any
): void {
    expect(fhirExtender.extendResource(resourceType, resource)).toEqual(
        expectedObject
    );
}

/** Helper method for testing minimizing a resource using {@link FhirExtender}. */
function testMinimizingResource(
    fhirExtender: FhirExtender,
    resourceType: string,
    resource: any,
    expectedObject: any
): void {
    expect(fhirExtender.minimizeResource(resourceType, resource)).toEqual(
        expectedObject
    );
}

/** Helper method for testing adding a default to a {@link FhirExtender}. */
function testAddingDefault(name: string, map: any, expectedObject: any): void {
    expect(createFhirExtenderWithDefault(name, map).getDefaults()).toEqual(
        expectedObject
    );
}

/** Helper method for testing adding defaults to a {@link FhirExtender}. */
function testAddingDefaults(map: any, expectedObject: any): void {
    expect(createFhirExtenderWithDefaults(map).getDefaults()).toEqual(
        expectedObject
    );
}

/** Creates a {@link FhirExtender} with the given default added. */
function createFhirExtenderWithDefault(name: string, map: any): FhirExtender {
    const fhirExtender = new FhirExtender();
    fhirExtender.addDefault(name, map);
    return fhirExtender;
}

/** Creates a {@link FhirExtender} with the given defaults added. */
function createFhirExtenderWithDefaults(map: any): FhirExtender {
    const fhirExtender = new FhirExtender();
    fhirExtender.addDefaults(map);
    return fhirExtender;
}
