import dataProvider from '../src/index';
import { ERROR_EXTRACT_LOCATION, ERROR_MISSING_HEADERS } from '../src/index';
import FhirExtender, { EMPTY_FHIR_EXTENDER } from '../src/FhirExtender';
import { SINGLE_PATIENT, PATIENT_BUNDLE } from './testdata';
import { fetchUtils, SORT_ASC, SORT_DESC } from 'ra-core';

describe('test dataProvider', () => {
    const BASE_URL = 'http://localhost:3000';
    const FORMAT_JSON_FILTER = '_format=json';
    const PATIENT_RESOURCE_TYPE = 'Patient';

    const FHIR_EXTENDER_PATIENT_BLOB = new FhirExtender();
    const BLOB_KEY = 'BLOB';
    const BLOB_VALUE = 'TEST';
    const PATIENT_WITH_BLOB = { ...SINGLE_PATIENT, [BLOB_KEY]: BLOB_VALUE };

    beforeAll(() => {
        FHIR_EXTENDER_PATIENT_BLOB.addDefault(PATIENT_RESOURCE_TYPE, {
            [BLOB_KEY]: BLOB_VALUE,
        });
    });

    test('minimal arguments for dataprovider', () => {
        const t = () => {
            dataProvider(BASE_URL)
                .getOne('Patient', { id: SINGLE_PATIENT.id })
                .then(response => response != undefined);
        };
        expect(t).not.toThrow(TypeError);
    });

    describe('getList', () => {
        const EXTRA_FILTER_KEY = 'KEY';
        const EXTRA_FILTER_VALUE = 'VALUE';

        test('test page offset,sort and return value', () => {
            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                PATIENT_BUNDLE
            );
            const params = {
                id: SINGLE_PATIENT.id,
                pagination: { page: 0, perPage: 3 },
                sort: { field: undefined, order: undefined },
                target: undefined,
                filter: {},
            };
            dataProvider(BASE_URL, httpClient)
                .getList(PATIENT_RESOURCE_TYPE, params)
                .then(response =>
                    expect(response.data).toEqual(
                        PATIENT_BUNDLE.entry.map(e => e.resource)
                    )
                );

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${FORMAT_JSON_FILTER}&_summary=count`
            );
        });

        test('test with patient id of style "Patient/1234", extra filter and test extending resources', () => {
            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                PATIENT_BUNDLE
            );
            const params = {
                id: 'Patient/' + SINGLE_PATIENT.id,
                pagination: { page: 0, perPage: 3 },
                sort: { field: 'id', order: 'DESC' },
                target: undefined,
                filter: { [EXTRA_FILTER_KEY]: EXTRA_FILTER_VALUE },
            };
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .getList(PATIENT_RESOURCE_TYPE, params)
                .then(response => {
                    expect(response.data.map(e => e[BLOB_KEY]).length).toEqual(
                        PATIENT_BUNDLE.entry.length
                    );

                    testCallToHTTPClient(
                        httpClient,
                        `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${EXTRA_FILTER_KEY}=${EXTRA_FILTER_VALUE}&${FORMAT_JSON_FILTER}&_summary=count`
                    );

                    testCallToHTTPClient(
                        httpClient,
                        `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${EXTRA_FILTER_KEY}=${EXTRA_FILTER_VALUE}&_sort=-_id&${FORMAT_JSON_FILTER}&_count=3&_page=0`
                    );
                });
        });

        test('Check for correct shiftPageOffset', () => {
            const PAGE_TEST_OFFSET = true;
            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                PATIENT_BUNDLE
            );
            const params = {
                id: SINGLE_PATIENT.id,
                pagination: { page: 1, perPage: 3 },
                sort: { field: undefined, order: undefined },
                target: undefined,
                filter: {},
            };
            dataProvider(
                BASE_URL,
                httpClient,
                FHIR_EXTENDER_PATIENT_BLOB,
                '_page',
                PAGE_TEST_OFFSET
            )
                .getList(PATIENT_RESOURCE_TYPE, params)
                .then(response => {
                    expect(response).toBeDefined();

                    testCallToHTTPClient(
                        httpClient,
                        `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${FORMAT_JSON_FILTER}&_count=3&_page=0`
                    );
                });
        });
    });

    describe('getOne', () => {
        const httpClient = createHTTPClientMock(200, undefined, undefined, {
            ...SINGLE_PATIENT,
        });

        test('with single Patient ID', () => {
            dataProvider(BASE_URL, httpClient, EMPTY_FHIR_EXTENDER)
                .getOne('Patient', { id: SINGLE_PATIENT.id })
                .then(response => {
                    expect(response.data.id).toEqual(SINGLE_PATIENT.id);
                });

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${SINGLE_PATIENT.id}?${FORMAT_JSON_FILTER}`
            );
        });

        test('test extending resources', () => {
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .getOne('Patient', { id: SINGLE_PATIENT.id })
                .then(response => {
                    expect(response.data[BLOB_KEY]).toEqual(BLOB_VALUE);
                });

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${SINGLE_PATIENT.id}?${FORMAT_JSON_FILTER}`
            );
        });

        test('test with patient id of style "Patient/1234"', () => {
            dataProvider(BASE_URL, httpClient, EMPTY_FHIR_EXTENDER)
                .getOne('Patient', { id: `Patient/${SINGLE_PATIENT.id}` })
                .then(response => {
                    expect(response.data.id).toEqual(SINGLE_PATIENT.id);
                });

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${SINGLE_PATIENT.id}?${FORMAT_JSON_FILTER}`
            );
        });
    });

    describe('getMany', () => {
        test('tests that the correct URL is called and resources are extended correctly', () => {
            const httpClient = createHTTPClientMock(200, undefined, undefined, {
                entry: [
                    { resource: { ...SINGLE_PATIENT } },
                    { resource: { ...SINGLE_PATIENT } },
                ],
            });
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .getMany(PATIENT_RESOURCE_TYPE, {
                    ids: [SINGLE_PATIENT.id, SINGLE_PATIENT.id],
                })
                .then(response =>
                    expect(response.data).toEqual([
                        PATIENT_WITH_BLOB,
                        PATIENT_WITH_BLOB,
                    ])
                );

            expect(httpClient).toHaveBeenCalledTimes(1);
            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${FORMAT_JSON_FILTER}&_id=${SINGLE_PATIENT.id},${SINGLE_PATIENT.id}`
            );
        });

        test("tests getMany for getting ids of type 'Patient/1234'", () => {
            const IDs = [1234, 4567];
            const httpClient = createHTTPClientMock(200, undefined, undefined, {
                entry: [],
            });
            dataProvider(BASE_URL, httpClient).getMany(PATIENT_RESOURCE_TYPE, {
                ids: [`Patient/${IDs[0]}`, `Patient/${IDs[1]}`],
            });

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${FORMAT_JSON_FILTER}&_id=${IDs[0]},${IDs[1]}`
            );
        });
    });

    describe('getManyReference', () => {
        const TARGET_NAME = 'Patient';
        const SORT_FIELD = 'sortfield';
        const EXTRA_FILTER_KEY = 'KEY';
        const EXTRA_FILTER_VALUE = 'VALUE';
        test('test page offset,sort and return value', () => {
            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                PATIENT_BUNDLE
            );
            const params = {
                id: SINGLE_PATIENT.id,
                pagination: { page: 0, perPage: 3 },
                sort: { field: SORT_FIELD, order: SORT_DESC },
                target: TARGET_NAME,
                filter: {},
            };
            dataProvider(BASE_URL, httpClient)
                .getManyReference(PATIENT_RESOURCE_TYPE, params)
                .then(response =>
                    expect(response.data).toEqual(
                        PATIENT_BUNDLE.entry.map(e => e.resource)
                    )
                );

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?_sort=-${SORT_FIELD}&${FORMAT_JSON_FILTER}&_count=3&_page=0&${TARGET_NAME}=1023`
            );
        });

        test('test with patient id of style "Patient/1234", extra filter and test extending resources', () => {
            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                PATIENT_BUNDLE
            );
            const params = {
                id: 'Patient/' + SINGLE_PATIENT.id,
                pagination: { page: 0, perPage: 3 },
                sort: { field: SORT_FIELD, order: SORT_ASC },
                target: TARGET_NAME,
                filter: { [EXTRA_FILTER_KEY]: EXTRA_FILTER_VALUE },
            };
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .getManyReference(PATIENT_RESOURCE_TYPE, params)
                .then(response =>
                    expect(response.data.map(e => e[BLOB_KEY]).length).toEqual(
                        PATIENT_BUNDLE.entry.length
                    )
                );

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${EXTRA_FILTER_KEY}=${EXTRA_FILTER_VALUE}&_sort=${SORT_FIELD}&${FORMAT_JSON_FILTER}&_count=3&_page=0&${TARGET_NAME}=1023`
            );
        });

        test('Check for correct shiftPageOffset', () => {
            const PAGE_TEST_OFFSET = true;
            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                PATIENT_BUNDLE
            );
            const params = {
                id: SINGLE_PATIENT.id,
                pagination: { page: 2, perPage: 3 },
                sort: { field: undefined, order: undefined },
                target: TARGET_NAME,
                filter: {},
            };
            dataProvider(
                BASE_URL,
                httpClient,
                FHIR_EXTENDER_PATIENT_BLOB,
                '_page',
                PAGE_TEST_OFFSET
            )
                .getManyReference(PATIENT_RESOURCE_TYPE, params)
                .then(response => response != undefined);

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/?${FORMAT_JSON_FILTER}&_count=3&_page=1&${TARGET_NAME}=1023`
            );
        });
    });

    describe('update', () => {
        const httpClient = createHTTPClientMock(200);

        test('test mimimize resource, URL check, check Method', () => {
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .update('Patient', {
                    id: PATIENT_WITH_BLOB.id,
                    data: PATIENT_WITH_BLOB,
                    previousData: undefined,
                })
                .then(response => {
                    expect(response.data[BLOB_KEY]).toEqual(undefined);
                    expect(response.data.id).toEqual(PATIENT_WITH_BLOB.id);
                });

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${SINGLE_PATIENT.id}`,
                JSON.stringify(SINGLE_PATIENT),
                'PUT'
            );
        });
    });

    describe('updateMany', () => {
        const httpClient = createHTTPClientMock(200);
        const IDs = [PATIENT_WITH_BLOB.id, 1, 2];

        test('test mimimize resource, URL check, check Method, test correct amount of calls', () => {
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .updateMany(PATIENT_RESOURCE_TYPE, {
                    ids: IDs,
                    data: PATIENT_WITH_BLOB,
                })
                .then(response => {
                    expect(response.data).toEqual(IDs);
                });

            for (const id in IDs) {
                testCallToHTTPClient(
                    httpClient,
                    `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${IDs[id]}`,
                    JSON.stringify(SINGLE_PATIENT),
                    'PUT'
                );
            }
            expect(httpClient).toHaveBeenCalledTimes(IDs.length);
        });
    });

    describe('create', () => {
        const NEW_ID = '42';
        const TEST_LOCATION = `https://virtlx164.aiim.med.tum.de/fhir-server/api/v4/Patient/${NEW_ID}/_history/`;
        var NEW_PATIENT = { ...SINGLE_PATIENT, id: NEW_ID };
        NEW_PATIENT['id'] = NEW_ID;

        test("test mimimize resource, URL check, check Method, test new ID from Header 'Location' ", () => {
            const httpClient = createHTTPClientMock(
                200,
                new Headers({ Location: TEST_LOCATION })
            );
            dataProvider(BASE_URL, httpClient, FHIR_EXTENDER_PATIENT_BLOB)
                .create(PATIENT_RESOURCE_TYPE, { data: PATIENT_WITH_BLOB })
                .then(response => {
                    expect(response.data[BLOB_KEY]).toBeUndefined();
                    expect(response.data.id).toEqual(NEW_ID);
                });

            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/`,
                JSON.stringify(SINGLE_PATIENT),
                'POST'
            );
        });

        test('no Headers in create has to die ', () => {
            const httpClient = createHTTPClientMock(200);
            const client = dataProvider(
                BASE_URL,
                httpClient,
                FHIR_EXTENDER_PATIENT_BLOB
            );
            //client.create("Patient", { data: PAT_WITH_BLOB }).catch(t => exp);
            expect(
                client.create('Patient', { data: PATIENT_WITH_BLOB })
            ).rejects.toEqual(ERROR_MISSING_HEADERS);
        });

        test('no Location Headers in create has to die ', () => {
            const httpClient = createHTTPClientMock(
                200,
                new Headers({ Test: 'Test' })
            );
            const client = dataProvider(
                BASE_URL,
                httpClient,
                FHIR_EXTENDER_PATIENT_BLOB
            );
            expect(
                client.create('Patient', { data: PATIENT_WITH_BLOB })
            ).rejects.toEqual(ERROR_MISSING_HEADERS);
        });

        test('No id in location header', () => {
            const httpClient = createHTTPClientMock(
                200,
                new Headers({ Location: '...' })
            );
            const client = dataProvider(
                BASE_URL,
                httpClient,
                FHIR_EXTENDER_PATIENT_BLOB
            );
            expect(
                client.create('Patient', { data: PATIENT_WITH_BLOB })
            ).rejects.toEqual(ERROR_EXTRACT_LOCATION);
        });
    });

    describe('delete', () => {
        test('tests that delete calls the httpClient with the correct URL and http method', () => {
            const httpClient = createHTTPClientMock(200);
            dataProvider(BASE_URL, httpClient).delete(PATIENT_RESOURCE_TYPE, {
                id: SINGLE_PATIENT.id,
            });
            testCallToHTTPClient(
                httpClient,
                `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${SINGLE_PATIENT.id}`,
                undefined,
                'DELETE'
            );
        });
    });

    describe('deleteMany', () => {
        test('tests that ', () => {
            // We have the same ID 3 times, as we can only mock a httpClient, that always returns the same Promise
            const IDs = [
                SINGLE_PATIENT.id,
                SINGLE_PATIENT.id,
                SINGLE_PATIENT.id,
            ];

            const httpClient = createHTTPClientMock(
                200,
                undefined,
                undefined,
                SINGLE_PATIENT
            );

            dataProvider(BASE_URL, httpClient)
                .deleteMany(PATIENT_RESOURCE_TYPE, { ids: IDs })
                .then(response => expect(response.data).toEqual(IDs));

            for (const id of IDs) {
                testCallToHTTPClient(
                    httpClient,
                    `${BASE_URL}/${PATIENT_RESOURCE_TYPE}/${id}`,
                    undefined,
                    'DELETE'
                );
            }

            expect(httpClient).toHaveBeenCalledTimes(IDs.length);
        });
    });
});

type HTTPClientMock = typeof fetchUtils.fetchJson;

type HTTPMethod =
    | 'GET'
    | 'HEAD'
    | 'POST'
    | 'PUT'
    | 'DELETE'
    | 'CONNECT'
    | 'OPTIONS'
    | 'TRACE'
    | 'PATCH';

/**
 * Helper method for creating a jest function which mocks a HTTP client.
 */
function createHTTPClientMock(
    expectedStatus: number,
    expectedHeaders?: Headers,
    expectedBody?: string,
    expectedJSON?: any
): HTTPClientMock {
    return jest.fn(() =>
        Promise.resolve({
            status: expectedStatus,
            headers: expectedHeaders,
            body: expectedBody,
            json: expectedJSON,
        })
    );
}

/**
 * Helper method for testing that a {@link HTTPClientMock} has been called with the correct url, body, and method.
 */
function testCallToHTTPClient(
    httpClient: HTTPClientMock,
    expectedURL: string,
    expectedBody?: string,
    expectedMethod?: HTTPMethod
) {
    if (expectedBody === undefined && expectedMethod === undefined) {
        expect(httpClient).toHaveBeenCalledWith(expectedURL);
    } else if (expectedBody !== undefined && expectedMethod === undefined) {
        expect(httpClient).toHaveBeenCalledWith(expectedURL, {
            body: expectedBody,
        });
    } else {
        expect(httpClient).toHaveBeenCalledWith(expectedURL, {
            body: expectedBody,
            method: expectedMethod,
        });
    }
}
