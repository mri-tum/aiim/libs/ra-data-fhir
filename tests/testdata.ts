export const SINGLE_PATIENT = {
    resourceType: 'Patient',
    id: '1023',
    meta: {
        versionId: '13',
        lastUpdated: '2022-07-08T20:42:30.450005Z',
        profile: [
            'http://hl7.org/fhir/us/core/StructureDefinition/us-core-patient',
        ],
    },
    text: {
        status: 'generated',
        div: '<div xmlns="http://www.w3.org/1999/xhtml">Generated by <a href="https://github.com/synthetichealth/synthea">Synthea</a>.Version identifier: master-branch-latest\n .   Person seed: -6106131635425746963  Population seed: 1652084601125</div>',
    },
    extension: [
        {
            extension: [
                {
                    url: 'ombCategory',
                    valueCoding: {
                        system: 'urn:oid:2.16.840.1.113883.6.238',
                        code: '2028-9',
                        display: 'Asian',
                    },
                },
                {
                    url: 'text',
                    valueString: 'Asian',
                },
            ],
            url: 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-race',
        },
        {
            extension: [
                {
                    url: 'ombCategory',
                    valueCoding: {
                        system: 'urn:oid:2.16.840.1.113883.6.238',
                        code: '2186-5',
                        display: 'Not Hispanic or Latino',
                    },
                },
                {
                    url: 'text',
                    valueString: 'Not Hispanic or Latino',
                },
            ],
            url: 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity',
        },
        {
            url: 'http://hl7.org/fhir/StructureDefinition/patient-mothersMaidenName',
            valueString: 'Mozelle144 Cremin516',
        },
        {
            url: 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex',
            valueCode: 'F',
        },
        {
            url: 'http://hl7.org/fhir/StructureDefinition/patient-birthPlace',
            valueAddress: {
                city: 'Ho Chi Minh City',
                state: 'Gia Định',
                country: 'VN',
            },
        },
        {
            url: 'http://synthetichealth.github.io/synthea/disability-adjusted-life-years',
            valueDecimal: 0.4288304931115761,
        },
        {
            url: 'http://synthetichealth.github.io/synthea/quality-adjusted-life-years',
            valueDecimal: 4.571169506888424,
        },
    ],
    identifier: [
        {
            system: 'https://github.com/synthetichealth/synthea',
            value: '1231f5e7-b0cb-08fe-1bf6-2b624fe9202a',
        },
        {
            type: {
                coding: [
                    {
                        system: 'http://terminology.hl7.org/CodeSystem/v2-0203',
                        code: 'MR',
                        display: 'Medical Record Number',
                    },
                ],
                text: 'Medical Record Number',
            },
            system: 'http://hospital.smarthealthit.org',
            value: '1231f5e7-b0cb-08fe-1bf6-2b624fe9202a',
        },
        {
            type: {
                coding: [
                    {
                        system: 'http://terminology.hl7.org/CodeSystem/v2-0203',
                        code: 'SS',
                        display: 'Social Security Number',
                    },
                ],
                text: 'Social Security Number',
            },
            system: 'http://hl7.org/fhir/sid/us-ssn',
            value: '999-39-3678',
        },
    ],
    name: [
        {
            use: 'official',
            family: 'Bailey598',
            given: ['Lurlene215'],
        },
    ],
    gender: 'female',
    birthDate: '1998-05-16',
    deceasedDateTime: '2004-06-21T01:05:46+02:00',
    address: [
        {
            extension: [
                {
                    extension: [
                        {
                            url: 'latitude',
                            valueDecimal: 42.370767880084884,
                        },
                        {
                            url: 'longitude',
                            valueDecimal: -71.02799231331038,
                        },
                    ],
                    url: 'http://hl7.org/fhir/StructureDefinition/geolocation',
                },
            ],
            line: ['629 Daugherty Orchard Apt 90'],
            city: 'Revere',
            state: 'MA',
            postalCode: '02151',
            country: 'US',
        },
    ],
    maritalStatus: {
        coding: [
            {
                system: 'http://terminology.hl7.org/CodeSystem/v3-MaritalStatus',
                code: 'S',
                display: 'Never Married',
            },
        ],
        text: 'Never Married',
    },
    multipleBirthBoolean: false,
    communication: [
        {
            language: {
                coding: [
                    {
                        system: 'urn:ietf:bcp:47',
                        code: 'vi',
                        display: 'Vietnamese',
                    },
                ],
                text: 'Vietnamese',
            },
        },
    ],
};

export const PATIENT_BUNDLE = {
    resourceType: 'Bundle',
    id: '280a83ce-2045-4503-930b-070b79b0ac3a',
    meta: {
        lastUpdated: '2022-07-13T10:44:51.859+00:00',
    },
    type: 'searchset',
    link: [
        {
            relation: 'self',
            url: 'https://hapi.fhir.org/baseR4/Patient',
        },
        {
            relation: 'next',
            url: 'https://hapi.fhir.org/baseR4?_getpages=280a83ce-2045-4503-930b-070b79b0ac3a&_getpagesoffset=20&_count=20&_pretty=true&_bundletype=searchset',
        },
    ],
    entry: [
        {
            fullUrl: 'https://hapi.fhir.org/baseR4/Patient/2151335',
            resource: {
                resourceType: 'Patient',
                id: '1023',
                meta: {
                    versionId: '2',
                    lastUpdated: '2022-03-24T23:48:02.473+00:00',
                    source: '#BT8CS8RNhOGiC8eS',
                },
                text: {
                    status: 'generated',
                    div: '<div xmlns="http://www.w3.org/1999/xhtml"><div class="hapiHeaderText">Perry <b>LYNNE </b></div><table class="hapiPropertyTable"><tbody/></table></div>',
                },
                name: [
                    {
                        family: 'deepak',
                        given: ['agarwal'],
                    },
                ],
                birthDate: '1963-12-26',
                deceasedBoolean: false,
                address: [
                    {
                        use: 'home',
                        type: 'both',
                        text: '534 Erewhon St PeasantVille, Rainbow,New York, Vic  67999',
                        line: ['534 Erewhon St'],
                        city: 'Lansdowne',
                        district: 'Nashvillae',
                        state: 'Vic',
                        postalCode: '399954JK',
                    },
                ],
            },
            search: {
                mode: 'match',
            },
        },
        {
            fullUrl: 'https://hapi.fhir.org/baseR4/Patient/2151301',
            resource: {
                resourceType: 'Patient',
                id: '1024',
                meta: {
                    versionId: '2',
                    lastUpdated: '2022-03-24T23:48:04.368+00:00',
                    source: '#tWhC15J0vm5nTjdL',
                },
                text: {
                    status: 'generated',
                    div: '<div xmlns="http://www.w3.org/1999/xhtml"><div class="hapiHeaderText">Perry <b>LYNNE </b></div><table class="hapiPropertyTable"><tbody/></table></div>',
                },
                name: [
                    {
                        family: 'deepak',
                        given: ['agarwal'],
                    },
                ],
                birthDate: '1963-12-26',
                deceasedBoolean: false,
                address: [
                    {
                        use: 'home',
                        type: 'both',
                        text: '534 Erewhon St PeasantVille, Rainbow,New York, Vic  67999',
                        line: ['534 Erewhon St'],
                        city: 'Lansdowne',
                        district: 'Nashvillae',
                        state: 'Vic',
                        postalCode: '399954JK',
                    },
                ],
            },
            search: {
                mode: 'match',
            },
        },
        {
            fullUrl: 'https://hapi.fhir.org/baseR4/Patient/2151304',
            resource: {
                resourceType: 'Patient',
                id: '1025',
                meta: {
                    versionId: '2',
                    lastUpdated: '2022-03-24T23:48:04.793+00:00',
                    source: '#nV9FukTRsvplN5yw',
                },
                text: {
                    status: 'generated',
                    div: '<div xmlns="http://www.w3.org/1999/xhtml"><div class="hapiHeaderText">Perry <b>LYNNE </b></div><table class="hapiPropertyTable"><tbody/></table></div>',
                },
                name: [
                    {
                        family: 'deepak',
                        given: ['agarwal'],
                    },
                ],
                birthDate: '1963-12-26',
                deceasedBoolean: false,
                address: [
                    {
                        use: 'home',
                        type: 'both',
                        text: '534 Erewhon St PeasantVille, Rainbow,New York, Vic  67999',
                        line: ['534 Erewhon St'],
                        city: 'Lansdowne',
                        district: 'Nashvillae',
                        state: 'Vic',
                        postalCode: '399954JK',
                    },
                ],
            },
            search: {
                mode: 'match',
            },
        },
    ],
};
