import {
    ID,
    buildFhirUrl,
    buildFhirUrlWithFilter,
    buildFhirUrlWithID,
    buildFhirUrlWithIDAndFilter,
} from '../src/FhirURLBuilder';

describe('tests buildFhirURL in FhirURLBuilder', () => {
    const BASE_URL = 'https://hapi.fhir.org/baseR4';
    const RESOURCE_TYPE = 'Patient';

    test('tests buildFhirUrl without passing any filters', () => {
        testBuildFhirUrl(
            BASE_URL,
            RESOURCE_TYPE,
            'https://hapi.fhir.org/baseR4/Patient/'
        );

        // Tests URL ending with '/'
        testBuildFhirUrl(
            BASE_URL + '/',
            RESOURCE_TYPE,
            'https://hapi.fhir.org/baseR4/Patient/'
        );

        testBuildFhirURLWithID(
            BASE_URL,
            RESOURCE_TYPE,
            '3045',
            'https://hapi.fhir.org/baseR4/Patient/3045'
        );
    });

    test("tests buildFhirUrl with passing filters that do not include operators like 'eq'", () => {
        testBuildFhirURLWithFilter(
            BASE_URL,
            RESOURCE_TYPE,
            {
                id: '3045',
                name: 'Max',
            },
            'https://hapi.fhir.org/baseR4/Patient/?id=3045&name=Max'
        );

        testBuildFhirURLWithIDAndFilter(
            BASE_URL,
            RESOURCE_TYPE,
            '3045',
            {
                _format: 'json',
            },
            'https://hapi.fhir.org/baseR4/Patient/3045?_format=json'
        );
    });

    test("tests buildFhirUrl with passing filters that include operators like 'eq'", () => {
        testBuildFhirURLWithFilter(
            BASE_URL,
            RESOURCE_TYPE,
            {
                id: '3045',
                name_ne: 'Max',
                age_ge: 20,
            },
            'https://hapi.fhir.org/baseR4/Patient/?id=3045&name=neMax&age=ge20'
        );

        testBuildFhirURLWithIDAndFilter(
            BASE_URL,
            RESOURCE_TYPE,
            '3045',
            {
                id: '3045',
                name_ne: 'Max',
                age_ge: 20,
            },
            'https://hapi.fhir.org/baseR4/Patient/3045?id=3045&name=neMax&age=ge20'
        );
    });
});

/** Tests that the function buildFhirUrl() returns expectedURL.*/
function testBuildFhirUrl(
    baseURL: string,
    resourceType: string,
    expectedURL: string
): void {
    expect(buildFhirUrl(baseURL, resourceType)).toEqual(expectedURL);
}

/**
 * @see testBuildFhirUrl
 */
function testBuildFhirURLWithID(
    baseURL: string,
    resourceType: string,
    id: ID,
    expectedURL: string
): void {
    expect(buildFhirUrlWithID(baseURL, resourceType, id)).toEqual(expectedURL);
}

/**
 * @see testBuildFhirUrl
 */
function testBuildFhirURLWithFilter(
    baseURL: string,
    resourceType: string,
    filter: any,
    expectedURL: string
): void {
    expect(buildFhirUrlWithFilter(baseURL, resourceType, filter)).toEqual(
        expectedURL
    );
}

/**
 * @see testBuildFhirUrl
 */
function testBuildFhirURLWithIDAndFilter(
    baseURL: string,
    resourceType: string,
    id: ID,
    filter: any,
    expectedURL: string
): void {
    expect(
        buildFhirUrlWithIDAndFilter(baseURL, resourceType, id, filter)
    ).toEqual(expectedURL);
}
