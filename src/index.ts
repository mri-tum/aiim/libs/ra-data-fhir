import { fetchUtils, DataProvider } from 'ra-core';
import {
    buildFhirUrlWithFilter,
    buildFhirUrlWithID,
    buildFhirUrlWithIDAndFilter,
    buildFhirUrl,
} from './FhirURLBuilder';
import FhirExtender from './FhirExtender';

export * from './FhirExtender';

export const ERROR_MISSING_HEADERS =
    'Could not create resource. No Headers, Location Header required.';
export const ERROR_EXTRACT_LOCATION =
    'Could not create resource. Could not extract ID from loaction header.';

/**
 * DataProvider for connecting react-admin and a FHIR server.
 * @param apiUrl is the url of the FHIR server
 * @param httpClient to use to execute the requests to the server (can include additional headers)
 * @param fhirExtender to fill missing fields of resources returned by the server. These fields are minimized in update() and create() calls to not change the data on the server.
 * @param pageOffsetKey different FHIR servers may have different conventions for specifying the current page (e.g. '_page' or 'page')
 * @param shiftPageOffset react-admin starts pages at index 1. Set this to true if your server starts pages at index 0.
 * @returns a dataProvider for making calls to a FHIR server
 */
export default (
    apiUrl: string,
    httpClient = fetchUtils.fetchJson,
    fhirExtender: FhirExtender = new FhirExtender(),
    pageOffsetKey: string = '_page',
    shiftPageOffset: boolean = false
): DataProvider => ({
    /**
     * Makes an API call to get multiple resources. The given filters are mapped to FHIR search.
     * @param resourceType the type of the resource (e.g. "Patient", "Observation")
     * @param params contains pagination and filters
     * @returns a list of resources matching pagination and filters
     */
    getList: (resourceType, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;

        // e.g HAPI-Fhir has a zero based page offset
        const pageOffset = shiftPageOffset ? page - 1 : page;

        const summaryUrl = buildFhirUrlWithFilter(apiUrl, resourceType, {
            ...params.filter,
            _format: 'json',
            _summary: 'count',
        });

        // only if the sort value is set, the sort is added to the filter values
        if (field != undefined) {
            params.filter['_sort'] = getSortFilter(field, order);
        }

        const resourceUrl = buildFhirUrlWithFilter(apiUrl, resourceType, {
            ...params.filter,
            _format: 'json',
            _count: perPage,
            [pageOffsetKey]: pageOffset,
        });

        return httpClient(summaryUrl).then(({ json }) => {
            const count = json.total;
            if (count == 0) {
                return {
                    data: [],
                    total: count,
                };
            }
            return httpClient(resourceUrl).then(({ json }) => {
                return {
                    data: json.entry.map(e =>
                        fhirExtender.extendResource(resourceType, e.resource)
                    ),
                    total: count,
                };
            });
        });
    },

    /**
     * Makes an API call to return one FHIR resource.
     * @param resourceType the type of the resource (e.g. "Patient", "Observation") to get
     * @param params contains the id of the resource to be fetched
     * @returns the FHIR resource corresponding to params.id
     */
    getOne: (resourceType, params) => {
        const resourceID = extractResourceIDFromFhirLink(
            params.id,
            resourceType
        );

        const url = buildFhirUrlWithIDAndFilter(
            apiUrl,
            resourceType,
            resourceID,
            {
                _format: 'json',
            }
        );

        return httpClient(url).then(({ json }) => {
            return {
                data: fhirExtender.extendResource(resourceType, json),
            };
        });
    },

    /**
     * Makes an API call to return multiple resource identified by ids.
     * @param resourceType the type of the resources (e.g. "Patient", "Observation") to get
     * @param params contains the ids of the resource to be fetched
     * @returns the resources corresponding to the id
     */
    getMany: (resourceType, params) => {
        const ids = params.ids.map(id =>
            extractResourceIDFromFhirLink(id, resourceType)
        );

        const url = buildFhirUrlWithFilter(apiUrl, resourceType, {
            _format: 'json',
            _id: ids.join(','),
        });

        return httpClient(url).then(({ json }) => ({
            data: json.entry.map(e =>
                fhirExtender.extendResource(resourceType, e.resource)
            ),
        }));
    },

    /**
     * Makes an API call to return resources that reference to another
     * @param resourceType the type of the resource (e.g. "Patient", "Observation") to get
     * @param params contains pagination and filters
     * @param params contains target, the value to merge (e.g. Patient in Observation)
     * @param params contains id, the value  which is expected in the target field
     * @returns related resources, e.g. All Observation which belong to Patient with id=1234
     */
    getManyReference: (resourceType, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;

        // e.g HAPI-Fhir has a zero based page offset
        const pageOffset = shiftPageOffset ? page - 1 : page;

        // only if the sort value is set, the sort is added to the filter values
        if (field != undefined) {
            params.filter['_sort'] = getSortFilter(field, order);
        }

        const resourceID = extractResourceIDFromFhirLink(
            params.id,
            params.target
        );

        const resourceUrl = buildFhirUrlWithFilter(apiUrl, resourceType, {
            ...params.filter,
            _format: 'json',
            _count: perPage,
            [pageOffsetKey]: pageOffset,
            [params.target]: resourceID,
        });

        return httpClient(resourceUrl).then(({ json }) => {
            return {
                data: json.entry.map(e =>
                    fhirExtender.extendResource(resourceType, e.resource)
                ),
            };
        });
    },

    /**
     * Makes an API call to update a specific resource.
     * @param resourceType the type of the resource (e.g. "Patient", "Observation") to be updated
     * @param params contains the id of the resource to update
     * @returns the updated resource including the ID of the resource
     */
    update: (resourceType, params) => {
        // minimize resources in case they were extended on load
        const minimizedData = fhirExtender.minimizeResource(
            resourceType,
            params.data
        );
        return httpClient(buildFhirUrlWithID(apiUrl, resourceType, params.id), {
            method: 'PUT',
            body: JSON.stringify(minimizedData),
        }).then(() => {
            return {
                data: { ...minimizedData, id: params.id },
            };
        });
    },

    /**
     * Makes an API call to update multiple resources with new data.
     * @param resourceType the type of the resources (e.g. "Patient", "Observation") to be updated
     * @param params contains the ids and data to update
     * @returns the ids of the updated resources
     */
    updateMany: (resourceType, params) =>
        Promise.all(
            params.ids.map(id => {
                // minimize resources in case they were extended on load
                const minimizedData = fhirExtender.minimizeResource(
                    resourceType,
                    params.data
                );
                return httpClient(
                    buildFhirUrlWithID(apiUrl, resourceType, id),
                    {
                        method: 'PUT',
                        body: JSON.stringify(minimizedData),
                    }
                );
            })
        ).then(() => ({ data: params.ids })),

    /**
     * Makes an API call to create a resource with the given data and resourceType.
     * @param resourceType the type of the resource (e.g. "Patient", "Observation") to be created
     * @param params contains the data for the resource, which should be created
     * @returns the created resource including the ID (which is returned by the server) of the resource
     */
    create: (resourceType, params) => {
        // minimize resources in case they were extended on load or through the form
        const minimizedData = fhirExtender.minimizeResource(
            resourceType,
            params.data
        );
        return httpClient(buildFhirUrl(apiUrl, resourceType), {
            method: 'POST',
            body: JSON.stringify(minimizedData),
        }).then(({ headers }) => {
            // Extract id from Location header
            // e.g. Location: https://localhost:3000/fhir-server/api/v4/Patient/181d904fe35-3ebec22e-b95c-4823-8363-85600c52c3af/_history/1
            if (headers == undefined || !headers.has('Location')) {
                return Promise.reject(ERROR_MISSING_HEADERS);
            }
            const urlElements = headers.get('Location').split('/');
            const resourceIndex = urlElements.findIndex(
                element => element === resourceType
            );
            if (resourceIndex < 0) {
                return Promise.reject(ERROR_EXTRACT_LOCATION);
            }
            const id = urlElements[resourceIndex + 1];

            return {
                data: { ...minimizedData, id: id },
            };
        });
    },

    /**
     * Makes an API call to delete the FHIR resource specified in params.id
     * @param resourceType the type of the resource (e.g. "Patient", "Observation") to be deleted
     * @param params contains the id of the resource, which should be deleted
     * @returns the deleted resource
     */
    delete: (resourceType, params) =>
        httpClient(buildFhirUrlWithID(apiUrl, resourceType, params.id), {
            method: 'DELETE',
        }).then(() => ({ data: params.previousData })),

    /**
     * Makes an API call to delete the FHIR resources specified in params.ids
     * @param resourceType the type of the resources (e.g. "Patient", "Observation") to be deleted
     * @param params which includes the ids to be deleted
     * @returns ids of the deleted resources
     */
    deleteMany: (resourceType, params) =>
        Promise.all(
            params.ids.map(id =>
                httpClient(buildFhirUrlWithID(apiUrl, resourceType, id), {
                    method: 'DELETE',
                })
            )
        ).then(responses => ({
            data: responses.map(({ json }) => json.id),
        })),
});

/**
 * @param field to sort by (given by react-admin to e.g. getList)
 * @param order is either DESC or ASC
 * @returns the URL filter for the FhirURLBuilder
 */
function getSortFilter(field: string, order: string): string {
    // Sorting by id in FHIR requires an underscore as prefix
    field = field === 'id' ? '_id' : field;
    // For descending sort order a minus symbol is used in FHIR
    return order.toLowerCase() == 'desc' ? '-' + field : field;
}

/**
 * In FHIR normally a reference is made by for example "Patient/1234". This function extracts only the ID.
 *
 * @param id which can be either <ID> or <{@link resourceType}>/<ID>
 * @param resourceType type of the FHIR resource
 * @returns the pure id (i.e. <ID> without <Resource> infront)
 */
function extractResourceIDFromFhirLink(
    id: string | number,
    resourceType: string
): string {
    return id.toString().replace(resourceType + '/', '');
}
