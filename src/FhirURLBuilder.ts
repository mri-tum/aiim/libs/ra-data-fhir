/**
 * Interface to clearly seperate the operator like < oder >
 * from the name and the value of the filter
 */
interface FhirFilter {
    name: string;
    operator?: FhirOperator;
    value: string;
}

/**
 * List of all possible operators in FHIR search.
 */
const fhirOperators = [
    'eq',
    'ne',
    'gt',
    'lt',
    'ge',
    'le',
    'sq',
    'eb',
    'ap',
] as const;

/**
 * Type, which represents all possible FHIR operators.
 */
type FhirOperator = typeof fhirOperators[number];

/**
 * Type for IDs
 */
export type ID = string | number;

/**
 * Builds a url conforming to the FHIR standard.
 *
 * @param baseURL which is the url up to the Resource (e.g. https://hapi.fhir.org/baseR4/)
 * @param resourceType which is the type of the resource (e.g. 'Patient', 'Observation')
 * @param filter is expected to be a object of key value pairs (e.g. {id: "1234", name_ne: "Max"}). Keys can have a {@link FhirOperator} seperated by '_' at the end.
 * @returns url with the baseURL, resource and name value pairs of the filter
 */
function _buildFhirUrl(
    baseURL: string,
    resourceType: string,
    id?: ID,
    filter?: any
): string {
    const nameValuePairs = parseFilter(filter)
        .map(
            ({ name, operator, value }) =>
                name + '=' + (operator ? operator : '') + value
        )
        .join('&');
    //format base URL
    let url = `${baseURL}${baseURL.endsWith('/') ? '' : '/'}`;
    //add resource type
    url += resourceType + '/';
    //add id
    url += id ? id : '';
    //add filters
    url += nameValuePairs === '' ? '' : '?' + nameValuePairs;
    return decodeURI(url);
}

/**
 * see {@link buildFhirUrl}
 */
export function buildFhirUrlWithID(
    baseURL: string,
    resourceType: string,
    id: ID
): string {
    return _buildFhirUrl(baseURL, resourceType, id, undefined);
}

/**
 * see {@link buildFhirUrl}
 */
export function buildFhirUrlWithIDAndFilter(
    baseURL: string,
    resourceType: string,
    id: ID,
    filter: any
): string {
    return _buildFhirUrl(baseURL, resourceType, id, filter);
}

/**
 * see {@link buildFhirUrl}
 */
export function buildFhirUrlWithFilter(
    baseURL: string,
    resourceType: string,
    filter: any
): string {
    return _buildFhirUrl(baseURL, resourceType, undefined, filter);
}

/**
 * see {@link buildFhirUrl}
 */
export function buildFhirUrl(baseURL: string, resourceType: string): string {
    return _buildFhirUrl(baseURL, resourceType, undefined, undefined);
}

/**
 * Extracts a {@link FhirFilter} by seperating the {@link name} into the actual {@link FhirFilter.name} and a {@link FhirOperator}.
 */
function extractFhirFilter(name: string, value: string): FhirFilter {
    for (const operator of fhirOperators) {
        const suffix = '_' + operator;
        if (name.endsWith(suffix)) {
            return createFhirFilter(
                name.slice(0, -suffix.length),
                value,
                operator
            );
        }
    }
    return createFhirFilter(name, value);
}

/**
 * Extracts a list of {@link FhirFilter} from the generic filter object.
 */
function parseFilter(filter: any): FhirFilter[] {
    if (filter === undefined || filter === null) {
        return [];
    }

    return Object.keys(filter).map(key => extractFhirFilter(key, filter[key]));
}

/**
 * Creates {@link FhirFilter} given a name, value and operator.
 */
function createFhirFilter(
    name: string,
    value: string,
    operator?: FhirOperator
): FhirFilter {
    return {
        name: name,
        operator: operator,
        value: value,
    };
}
