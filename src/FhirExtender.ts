import _ from 'lodash';

/**
 * Due to problems with non existing keys in the react-admin API,
 * this class can extend and minimize resources. This allows the developers
 * to define their own defaults. The defaults will be extended on every load
 * and will be minimized on every create/update.
 *
 * This is an optional use case. If you do not want to have your resource extended or minimized use
 * {@link EMPTY_FHIR_EXTENDER} without setting any defaults using {@link addDefault} or {@link addDefaults}.
 *
 */
export default class FhirExtender {
    private defaults: any = {};

    /**
     * Adds a default object for the given resource name
     *
     * @param resourceType type of resource (e.g Patient, Observation)
     * @param map object with default values for parameters of resource
     */
    addDefault(resourceType: string, map: any): void {
        if (
            resourceType === undefined ||
            map === undefined ||
            resourceType === null ||
            map == null
        ) {
            return;
        }
        this.defaults[resourceType] = map;
    }

    /**
     * Adds multiple objects for different resources
     *
     * e.g.
     * ```
     * {
     *  Patient: {id:0},
     *  Observation: {id:1}
     * }
     *
     * ```
     * Set the default ids for Patient and Observation simultaneously
     *
     * @param objectmap
     */
    addDefaults(objectmap: any): void {
        if (objectmap === undefined || objectmap === null) {
            return;
        }
        for (const key in objectmap) {
            this.defaults[key] = objectmap[key];
        }
    }

    /**
     * Extends the given resource with the declared default keys and values in {@link defaults}.
     * If and only if the given resource does not contain a default key, the default key value set will be added.
     *
     * @param resourceType type of the resource (e.g. "Patient", "Observation")
     * @param resource to be extended
     * @returns the extended resource
     */
    extendResource(resourceType: string, resource: any): any {
        if (!Object.keys(this.defaults).includes(resourceType)) {
            //do nothing
            return resource;
        } else {
            const def = this.defaults[resourceType];
            for (const key in def) {
                if (!Object.keys(resource).includes(key)) {
                    resource[key] = def[key];
                }
            }
            return resource;
        }
    }

    /**
     * Minimizes the given resource by removing fields that are declared in {@link defaults}.
     * Removes the fields if and only if the value is still the same as the default value.
     *
     * @param resourceType type of the resource (e.g Patient, Obervation)
     * @param resource to be minimized
     * @returns minimized resource
     */
    minimizeResource(resourceType: string, resource: any): any {
        //to make delete possible without any global side effects
        resource = this.deepCopyResource(resource);
        if (!Object.keys(this.defaults).includes(resourceType)) {
            //do nothing
            return resource;
        } else {
            const defaultsForName = this.defaults[resourceType];
            for (const key in defaultsForName) {
                if (Object.keys(resource).includes(key)) {
                    if (_.isEqual(resource[key], defaultsForName[key])) {
                        delete resource[key];
                    }
                }
            }
            return resource;
        }
    }

    /**
     * Makes a deep copy. See https://developer.mozilla.org/en-US/docs/Glossary/Deep_copy
     */
    private deepCopyResource(resource: any): any {
        return JSON.parse(JSON.stringify(resource));
    }

    /** Getter for the defaults */
    getDefaults(): any {
        return this.defaults;
    }
}

export const EMPTY_FHIR_EXTENDER: FhirExtender = new FhirExtender();
