# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2024-06-29

### Changed
-   update dependencies
-   add compatibility to React Admin v5


## [1.0.2] - 2023-03-10

### Changed
-   improved documentation
-   changelog format

## [1.0.1] - 2022-12-23

### Added
-   package.json meta data

## [1.0.0] - 2022-12-23

### Added
-   initial release, supporting [LinuxForHealth FHIR Server](https://github.com/LinuxForHealth/FHIR)