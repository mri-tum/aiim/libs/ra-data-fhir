# FHIR REST Data Provider For React-Admin

[![npm](https://img.shields.io/npm/v/@tum-mri-aiim/ra-data-fhir.svg)](https://www.npmjs.com/package/@tum-mri-aiim/ra-data-fhir)
[![Build status](https://gitlab.com/mri-tum/aiim/libs/ra-data-fhir/badges/master/pipeline.svg)](https://gitlab.com/mri-tum/aiim/libs/ra-data-fhir/-/pipelines?ref=master)
[![Test coverage](https://gitlab.com/mri-tum/aiim/libs/ra-data-fhir/badges/master/coverage.svg?job=test)](https://gitlab.com/mri-tum/aiim/libs/ra-data-fhir/-/jobs)
[![License: MIT](https://img.shields.io/gitlab/license/24181795)](https://gitlab.com/mri-tum/aiim/libs/ra-data-fhir/-/blob/master/LICENSE)

FHIR REST Data Provider for [react-admin](https://github.com/marmelab/react-admin). React-admin is a frontend framework for building admin applications. This dataProvider enables using a FHIR Rest Server with React-admin. The dataProvider was tested using an [LinuxForHealth](https://github.com/LinuxForHealth/FHIR) (formerly IBM FHIR Server).

## Installation

```sh
npm install --save @tum-mri-aiim/ra-data-fhir
```
## Testing

```sh
npm test
```

## Demo
Find a demo application using this data provider and a demo FHIR server setup [here](https://gitlab.com/mri-tum/aiim/demos/ra-data-fhir).

## REST Dialect
| Method             | API calls                                                                            |
| ------------------ | ------------------------------------------------------------------------------------ |
| `getList`          | `GET {baseURL}/{res}?{param=value}*`                                                 |
|                    | `GET {baseURL}/{res}?_summary=count&{param=value}*`                                  |
| `getOne`           | `GET {baseURL}/{res}/{id}`                                                           |
| `getMany`          | `GET {baseURL}/{res}/_id={val1,val2,val3}`                                           |
| `getManyReference` | `GET {baseURL}/{res}?target=value&{param=value}*`                                    |
| `create`           | `POST {baseURL}/{res} (id is returned by server in header)`                          |
| `update`           | `PUT {baseURL}/{res}/{id}`                                                           |
| `updateMany`       | `PUT {baseURL}/{res}/{id}` (called multiple times)                                   |
| `delete`           | `DELETE {baseURL}/{res}/{id}`                                                        |
| `deleteMany`       | `DELETE {baseURL}/{res}/{id}` (called multiple times)                                |

## Usage

See our example demo application for more information.

```jsx
import * as React from "react";
import { Admin, Resource, ListGuesser, ShowGuesser } from 'react-admin';
import dataProvider from 'ra-fhir';
import { EMPTY_FHIR_EXTENDER } from 'ra-fhir';
import { fetchUtils } from "react-admin";

const fetchJson = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    return fetchUtils.fetchJson(url, options);
}

const dp = dataProvider('rest', fetchJson, EMPTY_FHIR_EXTENDER);

const App = () => {
    return (
        <Admin  dataProvider={dp}>
              ....
        </Admin>
    )
};

export default App;
```

### Adding Custom Headers

The provider function accepts an HTTP client function as second argument. By default, they use react-admin's `fetchUtils.fetchJson()` as HTTP client. It's similar to HTML5 `fetch()`, except it handles JSON decoding and HTTP error codes automatically.

That means that if you need to add custom headers to your requests, you just need to _wrap_ the `fetchJson()` call inside your own function:

```jsx
import { fetchUtils, Admin, Resource } from 'react-admin';
import postgrestRestProvider from '@promitheus/ra-data-postgrest';

const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    // add your own headers here
    options.headers.set('X-Custom-Header', 'foobar');
    return fetchUtils.fetchJson(url, options);
};
const dataProvider = postgrestRestProvider('http://localhost:3000', httpClient);

render(
    <Admin dataProvider={dataProvider} title="Example Admin">
        ...
    </Admin>,
    document.getElementById('root')
);
```

Now all the requests to the REST API will contain the `X-Custom-Header: foobar` header.

**Tip**: The most common usage of custom headers is for authentication. `fetchJson` has built-on support for the `Authorization` token header:

```js
const httpClient = (url, options = {}) => {
    options.user = {
        authenticated: true,
        token: 'SRTRDFVESGNJYTUKTYTHRG',
    };
    return fetchUtils.fetchJson(url, options);
};
```

Now all the requests to the REST API will contain the `Authorization: SRTRDFVESGNJYTUKTYTHRG` header.

### Filter Operators

As FHIR allows several [comparators](https://www.hl7.org/fhir/search.html#prefix), e.g. `ne`, `gt`, `eq`...
The dataProvider is designed to enable you to specify the [filter operator](https://marmelab.com/react-admin/FilteringTutorial.html#filter-operators) in your react filter component:

```jsx
<Filter {...props}>
    <TextInput label="Search" source="name_ne" alwaysOn />
    <TextInput label="Search" source="age_gt" alwaysOn />
    // some more filters
</Filter>
```

One can simply append the comparator with an `_` to the source. In this example the field `name` would be filtered with `not equal` whereas `age` would be filtered using `greater than`. According to the FHIR standard, if no prefix is present, the prefix `eq` is assumed.


### FHIR Extender

The FHIR Extender can extend and minimize FHIR resources by adding or removing certain default attributes and values. In FHIR's resources, there are a lot of optional parameters. Thus, on the react-admin application side, these object attributes could potentially miss which could lead to a list full of non-uniform objects. The FHIR Extender solves this issue by extending the objects with defined key-value pairs.

```jsx
import { fetchUtils } from "react-admin";
import { EMPTY_FHIR_EXTENDER } from "@tum-mri-aiim/ra-data-fhir";

/**
 * Enable extending
 * The Patient resource is extended if the keys are absent.
 * E.g. if there is no deceasedDateTime the default will always be 01.01.1800
 */
EMPTY_FHIR_EXTENDER.addDefault("Patient", {
    name: [],
    address: [],
    deceasedDateTime: "1800-01-01",
});

const dp = dataProvider("rest", fetchUtils.fetchJson, EMPTY_FHIR_EXTENDER);
```

In this example, the `Patient` ressource is configured, to guarantee the fields `name`, `address` and `deceasedDateTime` with default values are present after a request. 

**Beware** that in the other direction (from the app to the server), the data will be minimized, i.e. a `Patient` which would be inserted with the date `1800-01-01` ends up having no date. Thus, one should wisely select default values.

![FHIR Extender](assets/FHIRExtender.png)

## Limitations

The present data provider is currently limited to run flawlessly in combination with the [LinuxForHealth FHIR server](https://github.com/LinuxForHealth/FHIR) (former IBM FRHIR Server). 
Compatibility with the [HAPI FHIR server](https://hapifhir.io/) is only given for read operations. Other FHIR servers, such as [Blaze](https://github.com/samply/blaze), were not tested yet. Please fell free to contribute concerning this compatibility. In future, we'd love to be the library to be compatible with many FHIR servers.

The current version of FHIR Extender cannot handle nested structures. There, it is currently required to create custom components. We plan to tackle this issue in future.

## License

This data provider is licensed under the MIT License.
